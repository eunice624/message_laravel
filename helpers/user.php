<?php

use Illuminate\Support\Facades\Auth;

if (!function_exists('get_current_login_user')) {
    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    function get_current_login_user()
    {
        return Auth::user();
    }
}

if (!function_exists('get_current_user_id')) {
    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    function get_current_user_id()
    {
        /**
         * @var $user \App\Model\User
         */
        $user = get_current_login_user();

        return $user ? $user->id : null;
    }
}