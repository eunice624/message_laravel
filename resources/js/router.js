import VueRouter from 'vue-router';
import Vue from 'vue';
Vue.use(VueRouter);

let routes = [
    {path: '/', name: 'index', component: require('./components/Home.vue').default},
    {path: '/new-message', name: 'new-message', component: require('./components/Message.vue').default},
    {path: '/js/popper.js.map', redirect: '/', component: require('./components/Home.vue').default},
];

const router = new VueRouter({
    // 依賴HTML5 History API和服務器配置。
    mode: 'history',
    //全局配置<router-link>默認的啟用的class。
    linkActiveClass: 'active',
    routes
});

export default router;