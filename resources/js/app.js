/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * vform see https://github.com/cretueusebiu/vform
 */
import {Form, HasError, AlertError} from 'vform';

window.Form = Form;
// Vue.component(HasError.name, HasError);
Vue.component('has-error', {
    props: ['form', 'field'],
    template: '<span class="typo__label" v-show="form.errors.has(field)">{{form.errors.get(field)}}</span>'
});
Vue.component(AlertError.name, AlertError);

/**
 * pagination
 */
Vue.component('pagination', require('laravel-vue-pagination'));

/**
 * sweetalert2
 */
import Swal from 'sweetalert2';

window.Swal = Swal;

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

window.Toast = Toast;

/** vue-router */
import router from './router';

const app = new Vue({
    el: '#app',
    router,
});
