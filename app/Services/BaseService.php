<?php
namespace App\Services;

use App\Support\Traits\Message;

abstract class BaseService
{
    use Message;
}