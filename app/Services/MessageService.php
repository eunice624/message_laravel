<?php

namespace App\Services;

use App\Model\Message;

class MessageService extends BaseService
{
    public function getMessage()
    {
        $messages = Message::with(['Creator']);

        return $messages->paginate(10);
    }

    public function createMessage($data)
    {
        $data['created_by'] = get_current_user_id();

        $result = Message::create($data);

        if ($result){
            return $this->success('新增留言成功');
        }else{
            return $this->error('新增留言失敗');
        }
    }
}