<?php

namespace App\Model;

use App\Model\EloquentBase as BaseModel;

class Message extends BaseModel
{
    protected $table = 'messages';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'content', 'created_by', 'updated_by'
    ];

    public $timestamps = true;

    public function Creator(){
        return $this->belongsTo('App\Model\User', 'created_by');
    }
}
