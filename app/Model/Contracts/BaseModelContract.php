<?php

namespace App\Model\Contracts;


interface BaseModelContract
{
    public function getPrimaryKey();

    public function getTable();

    public function expandFillable($attribute);

    public function unsetFillable($attribute);
}