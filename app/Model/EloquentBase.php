<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Contracts\BaseModelContract;

class EloquentBase extends Model implements BaseModelContract
{

    protected $primaryKey = false;


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getPrimaryKey()
    {
        return $this->getKeyName();
    }

    public function expandFillable($attribute)
    {
        $attributes = is_array($attribute) ? $attribute : func_get_args();

        $this->fillable = array_unique(array_merge($attributes, $this->fillable));

        return $this;
    }

    public function unsetFillable($attribute)
    {
        $attributes = is_array($attribute) ? $attribute : func_get_args();

        $this->fillable = array_filter($this->fillable, function ($item) use ($attributes) {
            return !in_array($item, $attributes);
        });

        return $this;
    }
}