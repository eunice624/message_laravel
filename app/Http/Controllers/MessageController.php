<?php

namespace App\Http\Controllers;

use App\Http\Requests\Message\MessageRequest;
use App\Services\MessageService;

class MessageController extends Controller
{
    protected $service;

    /**
     * MessageController constructor.
     * @param MessageService $service
     */
    public function __construct(MessageService $service)
    {
        $this->service = $service;

    }

    /**
     * @return array
     */
    public function index()
    {
        return [
            'messages' => $this->service->getMessage()
        ];
    }

    /**
     * @param MessageRequest $request
     * @return array
     */
    public function create(MessageRequest $request)
    {
        if( ! get_current_login_user()){
            return response_with_messages('請先登入再新增留言', true, \Constants::FORBIDDEN_CODE);
        }
        return $this->service->createMessage($request->all());
    }
}
