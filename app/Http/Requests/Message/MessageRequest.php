<?php

namespace App\Http\Requests\Message;

use App\Http\Requests\Request;

class MessageRequest extends Request
{
    public function rules()
    {
        return [
            'title' => 'required|string|max:60',
            'content' => 'required|string|min:20|max:10000000',
        ];
    }

    public function attributes()
    {
        return [
            'title' => '標題',
            'content' => '內容'
        ];
    }
}
