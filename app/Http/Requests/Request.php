<?php
/**
 * Created by PhpStorm.
 * User: kin
 * Date: 13/11/2018
 * Time: 上午10:56
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    abstract function rules();
}
