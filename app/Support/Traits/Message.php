<?php

namespace App\Support\Traits;

trait Message
{
    protected function error($message = null, array $data = null): array
    {
        if (!$message) {
            $message = '發生錯誤';
        }

        return response_with_messages($message, true, \Constants::ERROR_CODE, $data);
    }

    protected function success($message = null, array $data = null): array
    {
        if (!$message) {
            $message = '操作成功';
        }

        return response_with_messages($message, false, \Constants::SUCCESS_CODE, $data);
    }
}