<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');

    Route::group(['prefix' => 'api'], function () {
        Route::group(['prefix' => 'message'], function () {
            Route::get('/', 'MessageController@index');

            Route::post('/', 'MessageController@create');
        });
    });

    Route::get('{path}', 'HomeController@index')->where('path', '([A-z\d-/_.]+)?');
});